﻿using UnityEngine;
using System.Collections;


namespace ExampleProject
{
    public class InteractiveObject : MonoBehaviour
    {


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }


        public virtual void Interact()
        {
            Debug.Log("Interacting");
        }

    }
}