﻿using UnityEngine;
using System.Collections;



namespace ExampleProject
{
    public class Button : InteractiveObject
    {
        //public int numInteractions = 0;
        
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void Interact()
        {
            base.Interact();
            Debug.Log("You pressed the button!!!");
            var color = GetComponent<SpriteRenderer>().color;
            if(color == Color.red)
            {
                GetComponent<SpriteRenderer>().color = Color.blue;
                
            }
            else
            {
                GetComponent<SpriteRenderer>().color = Color.red;
            }
        }
    }
}