﻿using UnityEngine;
using System.Collections;

namespace ExampleProject
{
    public class Player : MonoBehaviour
    {
        InteractiveObject interactiveObj = null;

        // Use this for initialization
        void Start()
        {

        }

        
        void FixedUpdate()
        {
            if(interactiveObj != null && Input.GetKeyDown(KeyCode.Z))
            {
                interactiveObj.Interact();
            }
        }

        void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.GetComponent<InteractiveObject>() != null)
            {
                interactiveObj = coll.GetComponent<InteractiveObject>();
            }
        }


        void OnTriggerExit2D(Collider2D coll)
        {
            if (coll.GetComponent<InteractiveObject>() != null)
            {
                interactiveObj = null;
            }
        }
    }
}